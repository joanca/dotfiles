#!/bin/bash
# vim: set ft=sh sw=4 et:

MODE="$1"

if [[ $MODE == "commands" ]]; then
    COMMAND="compgen -c | sort -u | fzf --no-extended --print-query --bind ctrl-n:replace-query | tail -n1"
elif [[ $MODE == "pass" ]]; then
    COMMAND="find $HOME/.config/password-store -type f -name '*.gpg' -printf '%P\\n' | awk '{ print substr(\$0, 0, length(\$0)-4) }' | fzf --bind ctrl-n:replace-query"
else
    exit 1
fi

TMP=$(mktemp)

alacritty --class "alacritty-menu" -e bash -c "$COMMAND > $TMP"

cat "$TMP"
rm "$TMP"
