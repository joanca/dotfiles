#!/usr/bin/sh
cd ~/Wallpapers

image1=$(ls -d $PWD/* | shuf -n 1)
image2=$(ls -d $PWD/* | shuf -n 1)

swaymsg output eDP-1 bg "$image1" fill
swaymsg output DP-1 bg "$image2" fill
