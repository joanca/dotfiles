#!/usr/bin/sh
cd ~/Wallpapers

# Grab one image randomly
image1=$(ls -d $PWD/* | shuf -n 1)
image2=$(ls -d $PWD/* | shuf -n 1)

# Create temporary images
tmp_wall1="/tmp/swaylock_wallpaper_1.jpeg"
tmp_wall2="/tmp/swaylock_wallpaper_2.jpeg"

# Add blur mask over images and save to tmp variables
ffmpeg -i "$image1" -filter_complex boxblur=lr=20:lp=2 -y "$tmp_wall1"
ffmpeg -i "$image2" -filter_complex boxblur=lr=20:lp=2 -y "$tmp_wall2"

# lock screen with blurred image
swaylock -i "eDP-1:$tmp_wall1" -i "DP-1:$tmp_wall2" -n
