function bun -d "bun inside docker container"
    docker run --rm \
        --init \
        --ulimit memlock=-1:-1 \
        --network host \
        -v $(pwd):/home/bun/app \
        -v $(basename $(pwd))_node_modules:/home/bun/app/node_modules \
        oven/bun $argv
end
