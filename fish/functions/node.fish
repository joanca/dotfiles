function node -d "node inside docker container"
    set CONTAINER_NAME $(basename $(pwd))

    docker run -d --rm \
        --name $CONTAINER_NAME \
        --init \
        --network host \
        --ulimit memlock=-1:-1 \
        -w /home/node/app \
        -v $(pwd):/home/node/app \
        -v {$CONTAINER_NAME}_node_modules:/home/node/app/node_modules \
        node:21-slim sleep infinity > /dev/null 2>&1

    docker exec $CONTAINER_NAME $argv
end
