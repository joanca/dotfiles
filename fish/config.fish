if status is-interactive
    and not set -q TMUX
    exec tmux
end

# Set PATH
source ~/.config/fish/path.fish

# Set aliases
source ~/.config/fish/alias/git.fish
source ~/.config/fish/alias/archlinux.fish
