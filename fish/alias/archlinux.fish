alias sway="env GDK_BACKEND=wayland QT_QPA_PLATFORM=xcb MOZ_ENABLE_WAYLAND=1 sway"
alias tor="sudo -u tor /usr/bin/tor"
alias yarn="node yarn"
alias npm="node npm"
alias npx="node npx"

alias logout="tmux detach && exit"
