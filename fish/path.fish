set -gx PATH $PATH $HOME/.cargo/bin
set -gx PATH $PATH $HOME/.yarn/bin
set -gx PATH $PATH $HOME/.jenv/bin
