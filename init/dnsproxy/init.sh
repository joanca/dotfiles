#!/bin/sh
exec /usr/sbin/sshd -D &

# Start tailscale
sh ./tailscale.sh &

./dnsproxy/dnsproxy --config-path=/config.yaml
