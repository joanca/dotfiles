# Start tailscale
sudo /usr/sbin/tailscaled --state=/var/lib/tailscale/tailscaled.state --socket=/run/tailscale/tailscaled.sock --port 41641 &

sleep 5 # wait for tailscale daemon to be loaded
sudo tailscale up --auth-key=$TAILSCALE_KEY --ssh --force-reauth
