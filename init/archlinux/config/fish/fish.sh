# Install
## install fish
curl -sL https://raw.githubusercontent.com/jorgebucaran/fisher/main/functions/fisher.fish | source && fisher install jorgebucaran/fisher

## adding plugins
fisher install jorgebucaran/nvm.fish

## copy config
mkdir -p .config/fish/functions
cp -r dotfiles/fish/alias ~/.config/fish/alias
cp dotfiles/fish/functions/fish_prompt.fish ~/.config/fish/functions/fish_prompt.fish
cp dotfiles/fish/config.fish ~/.config/fish/config.fish
cp dotfiles/fish/path.fish ~/.config/fish/path.fish

# Setup
## set default node
nvm install 16
nvm use 16
npm install -g yarn

set --universal nvm_default_version v16
