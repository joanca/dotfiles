# Remove setup files
rm -rf dotfiles

# Start Tailscale VPN
fish /tmp/config/vpn/tailscale.sh &

# run PIA VPN
#cd ~/.config/pia-vpn
#sudo VPN_PROTOCOL=wireguard DISABLE_IPV6=yes AUTOCONNECT=true PIA_PF=false MAX_LATENCY=1 PIA_USER=$PIA_USER PIA_PASS=$PIA_PASS ./run_setup.sh
#cd

# run remote terminal with tmux
ttyd -p 8484 -t fontSize=18 -t rendererType=webgl tmux new-session -A -s main
