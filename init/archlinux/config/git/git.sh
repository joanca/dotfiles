# copy gitconfig
cp /tmp/config/git/gitconfig ~/.gitconfig

# set user
git config --global user.email "jcaravenae@gmail.com"
git config --global user.name "joanca"

# set editor
git config --global core.editor vim

# pull rebase by default
git config --global pull.rebase true


